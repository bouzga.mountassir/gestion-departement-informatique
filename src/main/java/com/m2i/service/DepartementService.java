package com.m2i.service;

import com.m2i.dto.DepartementDto;

public interface DepartementService {
    /**
     * Chercher un departement par son id
     * @param id
     * @return
     */
    DepartementDto findById(Long id);

    /**
     * Ajouter un departement
     * @param departement
     * @return
     */

    DepartementDto save(DepartementDto departement);

    /**
     * Supprimer un utilisateur en utilisant son id
     * @param idUtilisateur
     */
    void deleteUtilisateurFonction(Long idUtilisateur);

}
