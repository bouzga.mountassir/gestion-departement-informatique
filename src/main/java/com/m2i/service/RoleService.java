package com.m2i.service;

import com.m2i.dto.RoleDto;

/**
 * Les roles affecter aux utilisateurs
 */

public interface RoleService {

    RoleDto findRoleByNom(String nom);
}
