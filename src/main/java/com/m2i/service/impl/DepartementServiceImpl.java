package com.m2i.service.impl;

import com.m2i.dao.DepartementRepository;
import com.m2i.dto.DepartementDto;
import com.m2i.entities.Departement;
import com.m2i.service.DepartementService;
import com.m2i.utils.DepartementUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DepartementServiceImpl implements DepartementService {
    private final DepartementRepository departementRepository;

    public DepartementServiceImpl(DepartementRepository departementRepository) {
        this.departementRepository = departementRepository;
    }

    @Override
    public DepartementDto findById(Long id) {
        return DepartementUtils.getInstance().mapEntityToDto(departementRepository.findById(id).orElse(null));
    }

    @Override
    public DepartementDto save(DepartementDto departement) {
        return DepartementUtils.getInstance().mapEntityToDto(
                departementRepository.save(DepartementUtils.getInstance().mapDtoToEntity(departement)));
    }

    @Override
    public void deleteUtilisateurFonction(Long idUtilisateur) {
        Departement departement = departementRepository.findByUtilisateurId(idUtilisateur);
        if (departement != null) {
            if(departement.getChef().getIdUtilisateur().equals(idUtilisateur)){
                departement.setChef(null);
            } else if (departement.getAdjoint().getIdUtilisateur().equals(idUtilisateur)) {
                departement.setAdjoint(null);
            } else if (departement.getTechnicien().getIdUtilisateur().equals(idUtilisateur)) {
                departement.setTechnicien(null);
            }
            departementRepository.saveAndFlush(departement);
        }
    }
}
