package com.m2i.service.impl;

import com.m2i.dao.FournitureRepository;
import com.m2i.dto.FournitureDto;
import com.m2i.dto.MaterielDto;
import com.m2i.entities.Fourniture;
import com.m2i.entities.Materiel;
import com.m2i.service.FournitureService;
import com.m2i.utils.FournitureUtils;
import com.m2i.utils.MaterielUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class FournitureServiceImpl implements FournitureService {

    private final FournitureRepository fournitureRepository;

    public FournitureServiceImpl(FournitureRepository fournitureRepository) {
        this.fournitureRepository = fournitureRepository;
    }

    @Override
    public FournitureDto findById(Long id) {
        return FournitureUtils.getInstance().mapEntityToDto(fournitureRepository.findById(id).orElse(null));
    }

    @Override
    public FournitureDto save(FournitureDto dto) {
        return FournitureUtils.getInstance().mapEntityToDto(
                fournitureRepository.save(FournitureUtils.getInstance().mapDtoToEntity(dto)));
    }

    @Override
    public void delete(Long id) {
        fournitureRepository.deleteDemandeByFourniture(id);
        fournitureRepository.deleteById(id);
    }

    @Override
    public List<FournitureDto> findAll() {
        return FournitureUtils.getInstance().mapEntityToDtoList(fournitureRepository.findAll());
    }

    @Override
    public List<FournitureDto> findByType(String type) {
        return FournitureUtils.getInstance().mapEntityToDtoList(fournitureRepository.findByType(type));
    }

    @Override
    public FournitureDto update(FournitureDto dto) {
        Fourniture entity = fournitureRepository.findById(dto.getIdFourniture()).orElse(null);
        FournitureDto fournitureDto = FournitureUtils.getInstance().populateDto(dto,entity);
        return save(fournitureDto);
    }

    @Override
    public void updateQuantite(Long id, Integer quantite) {
        fournitureRepository.updateQuantite(id,quantite);
    }

}
