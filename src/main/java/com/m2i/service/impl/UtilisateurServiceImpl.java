package com.m2i.service.impl;

import com.m2i.dao.RoleRepository;
import com.m2i.dao.UtilisateurRepository;
import com.m2i.dto.DepartementDto;
import com.m2i.dto.RoleDto;
import com.m2i.dto.UtilisateurDto;
import com.m2i.entities.Role;
import com.m2i.entities.Utilisateur;
import com.m2i.enums.RoleName;
import com.m2i.service.DemandeService;
import com.m2i.service.DepartementService;
import com.m2i.service.MaterielService;
import com.m2i.service.UtilisateurService;
import com.m2i.utils.UtilisateurUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {

    private final UtilisateurRepository utilisateurRepository;

    private final MaterielService materielService;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    private final DepartementService departementService;

    public UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository, MaterielService materielService, PasswordEncoder passwordEncoder, RoleRepository roleRepository, DepartementService departementService) {
        this.utilisateurRepository = utilisateurRepository;
        this.materielService = materielService;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.departementService = departementService;
    }

    @Override
    public UtilisateurDto findById(Long id) {
        return UtilisateurUtils.getInstance().mapEntityToDto(
                utilisateurRepository.findById(id).orElse(null));
    }

    @Override
    public List<UtilisateurDto> findByAll() {
        return UtilisateurUtils.getInstance()
                .mapEntityLToDtoList(utilisateurRepository.findAll());
    }

    @Override
    public UtilisateurDto save(UtilisateurDto dto) {

        if(checkIfUserExist(dto.getEmail())){
            throw new RuntimeException("Un utilisateur existe déjà avec cet email");
        }

        if(dto.getPassword() != null){
            dto.setPassword(passwordEncoder.encode(dto.getPassword()));
        }
        return UtilisateurUtils.getInstance().mapEntityToDto(utilisateurRepository.save(UtilisateurUtils.getInstance().mapDtoToEntity(dto)));
    }

    @Override
    public UtilisateurDto update(UtilisateurDto dto) {
        Utilisateur entity = utilisateurRepository.findById(dto.getIdUtilisateur()).orElse(null);
        UtilisateurDto userDto = UtilisateurUtils.getInstance().populateDto(dto,entity);
        return UtilisateurUtils.getInstance().mapEntityToDto(utilisateurRepository.save(UtilisateurUtils.getInstance().mapDtoToEntity(userDto)));
    }

    @Override
    /**
     * Spprimer un utilisateur en utilisant son id
     * supprimer les affectations de matériel liées à cet utilisateur
     * supprimer les fonctions de l'utilisateur dans les département
     * supprimer les demandes liéees à cet utilisateur
     */
    public void delete(Long id) {
        materielService.deleteAffectationMateriel(id);
        departementService.deleteUtilisateurFonction(id);
        utilisateurRepository.deleteDemandeByUtilisateur(id);
        utilisateurRepository.delteUserCustom(id);
    }

    @Override
    public List<UtilisateurDto> findByName(String nom) {
        return UtilisateurUtils.getInstance()
                .mapEntityLToDtoList(utilisateurRepository.findByName(nom));
    }

    @Override
    public UtilisateurDto addRoles(Long idUtilisateur, List<RoleDto> roles) {
        Utilisateur utilisateur = utilisateurRepository.findById(idUtilisateur).orElse(null);
        List<Role> rolesEntities = getRolesEntities(roles);
        utilisateur.getRoles().addAll(rolesEntities);
        return UtilisateurUtils.getInstance().mapEntityToDto(utilisateur);
    }

    @Override
    public UtilisateurDto findUtilisateurByEmail(String email) {
        return UtilisateurUtils.getInstance().mapEntityToDto(
                utilisateurRepository.findUtilisateurByEmail(email));
    }

    @Override
    public UtilisateurDto loadUserByUsername(String username) {
        return findUtilisateurByEmail(username);
    }

    private List<Role> getRolesEntities(List<RoleDto> roles){
        List<Role> rolesEntities = null;
        if(roles != null && !roles.isEmpty()){
            List<RoleName> roleName = new ArrayList<>();
            roles.forEach(r -> roleName.add(r.getNom()));
            rolesEntities = roleRepository.findRoleListByNameList(roleName);
        }
        return rolesEntities;
    }

    private List<Role> getUserRolesByAdmin(boolean isAdmin){
        List<RoleName> roles = new LinkedList<>(Arrays.asList(RoleName.UTILISATEUR));
        if(isAdmin){
            roles.add(RoleName.ADMINISTRATEUR);
        }
        return roleRepository.findRoleListByNameList(roles);
    }

    private boolean checkIfUserExist(String email){
        return findUtilisateurByEmail(email) != null;
    }

    @Override
    public DepartementDto updateChefDepartement(Long idDepartement, Long idChef) {

        DepartementDto departementDto = departementService.findById(idDepartement);

        Utilisateur nouveauChef = utilisateurRepository.findById(idChef).orElse(null);
        Utilisateur ancienChef = utilisateurRepository.findById(departementDto.getChef().getIdUtilisateur()).orElse(null);

        // Ajouter le role ADMIN pour le nouveau chef
        nouveauChef.setRoles(getUserRolesByAdmin(true));
        utilisateurRepository.save(nouveauChef);

        // Supprimer le role ADMIN pour l'ancien chef
        if(!departementDto.getAdjoint().getIdUtilisateur().equals(ancienChef.getIdUtilisateur())) {
            ancienChef.setRoles(getUserRolesByAdmin(false));
        }
        utilisateurRepository.save(ancienChef);

        departementDto.setChef(UtilisateurUtils.getInstance().mapEntityToDto(nouveauChef));

        return departementService.save(departementDto);
    }

    @Override
    public DepartementDto updateChefAdjointDepartement(Long idDepartement, Long idAdjoint) {
        DepartementDto departementDto = departementService.findById(idDepartement);

        Utilisateur nouveauAdjoint = utilisateurRepository.findById(idAdjoint).orElse(null);
        Utilisateur ancienAdjoint = utilisateurRepository.findById(departementDto.getAdjoint().getIdUtilisateur()).orElse(null);

        // Ajouter le role ADMIN pour le nouveau adjoint
        nouveauAdjoint.setRoles(getUserRolesByAdmin(true));
        utilisateurRepository.save(nouveauAdjoint);

        // Supprimer le role ADMIN pour l'ancien adjoint
        if(!departementDto.getChef().getIdUtilisateur().equals(ancienAdjoint.getIdUtilisateur())){
            ancienAdjoint.setRoles(getUserRolesByAdmin(false));
        }
        utilisateurRepository.save(ancienAdjoint);

        departementDto.setAdjoint(UtilisateurUtils.getInstance().mapEntityToDto(nouveauAdjoint));

        return departementService.save(departementDto);
    }

    @Override
    public UtilisateurDto updatePassword(UtilisateurDto dto) {
        String password = passwordEncoder.encode(dto.getPassword());
        utilisateurRepository.updatePassword(dto.getIdUtilisateur(), password);
        return findById(dto.getIdUtilisateur());
    }
}
