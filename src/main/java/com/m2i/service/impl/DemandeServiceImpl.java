package com.m2i.service.impl;

import com.m2i.dao.DemandeRepository;
import com.m2i.dto.DemandeDto;
import com.m2i.dto.FournitureDto;
import com.m2i.dto.MaterielDto;
import com.m2i.dto.UtilisateurDto;
import com.m2i.entities.Demande;
import com.m2i.entities.Utilisateur;
import com.m2i.enums.StatutDemande;
import com.m2i.enums.TypeDemande;
import com.m2i.service.DemandeService;
import com.m2i.service.FournitureService;
import com.m2i.service.MaterielService;
import com.m2i.service.UtilisateurService;
import com.m2i.utils.DemandeUtils;
import com.m2i.utils.UtilisateurUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DemandeServiceImpl implements DemandeService {

    private final DemandeRepository demandeRepository;
    private final MaterielService materielService;
    private final FournitureService fournitureService;
    private final UtilisateurService utilisateurService;

    @Override
    public DemandeDto findById(Long id) {
        return DemandeUtils.getInstance().mapEntityToDto(demandeRepository.findById(id).orElse(null));
    }

    @Override
    public List<DemandeDto> findAll() {
        return DemandeUtils.getInstance().mapEntityToDtoList(demandeRepository.findAll());
    }

    @Override
    public List<DemandeDto> findByStatut(String statut) {
        return DemandeUtils.getInstance().mapEntityToDtoList(demandeRepository.findByStatut(StatutDemande.valueOf(statut)));
    }

    @Override
    public List<DemandeDto> findByDemandeur(Long id) {
        return DemandeUtils.getInstance().mapEntityToDtoList(demandeRepository.findByDemandeur(id));
    }

    @Override
    public DemandeDto save(DemandeDto dto) {
        List<Demande> demandes = demandeRepository.findDemandeByUserAndTypeAndObject(dto.getIdDemandeur(),
                TypeDemande.valueOf(dto.getType()),
                dto.getMateriel() != null ? dto.getMateriel().getIdMateriel() : dto.getFourniture().getIdFourniture());
        // S'il existe une demande ENCOURS pour le meme materiel ou fourniture on déclenche une exception
        if (demandes != null && !demandes.isEmpty()) {
            demandes.stream().forEach(d -> {
                if (d.getStatut().equals(StatutDemande.ENCOURS)) {
                    if(TypeDemande.valueOf(dto.getType()).equals(TypeDemande.MATERIEL)){
                        throw new RuntimeException("Vous avez déjà demandé ce materiél");
                    } else {
                        throw new RuntimeException("Vous avez déjà demandé cette fourniture");
                    }
                }
            });
        }
        UtilisateurDto demandeur = utilisateurService.findById(dto.getIdDemandeur());
        dto.setDateDemande(LocalDateTime.now().toString());
        dto.setDemandeur(demandeur);
        dto.setStatut(StatutDemande.ENCOURS.name());
        switch (TypeDemande.valueOf(dto.getType())){
            case MATERIEL:
                MaterielDto materielDto = materielService.findById(dto.getMateriel().getIdMateriel());
                DateTimeFormatter formattersFront = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                String dateAcquisition = LocalDate.parse(materielDto.getDateAcquisition(),formattersFront)
                        .atTime(0,0,1)
                        .toString().replace("T"," ");
                materielDto.setDateAcquisition(dateAcquisition);
                dto.setMateriel(materielDto);
                break;
            case FOURNITURE:
                FournitureDto fournitureDto = fournitureService.findById(dto.getFourniture().getIdFourniture());
                if(dto.getQuantite() > fournitureDto.getQuantite()){
                    throw new RuntimeException("La quantité demandée est indisponible");
                }
                dto.setFourniture(fournitureDto);
                break;
            default:
                break;
        }
        Demande enitity = DemandeUtils.getInstance().mapDtoToEntity(dto);
        DemandeDto result = DemandeUtils.getInstance().mapEntityToDto(
                demandeRepository.save(enitity));
        return result;
    }

    @Override
    public DemandeDto update(DemandeDto dto) {
        return null;
    }

    @Override
    public void traiterDemande(DemandeDto dto) {
        Utilisateur approbateur = UtilisateurUtils.getInstance().mapDtoToEntity(utilisateurService.findById(dto.getIdApprobateur()));

        Demande demande = demandeRepository.findById(dto.getIdDemande()).orElse(null);
        demande.setApprobateur(approbateur);
        demande.setDateTraitement(LocalDateTime.now());
        demande.setStatut(StatutDemande.valueOf(dto.getStatutCible()));
        demandeRepository.save(demande);
        if(demande.getStatut().equals(StatutDemande.APPROUVEE)){
            if(demande.getMateriel() != null){
                materielService.affecterMateriel(demande.getMateriel().getIdMateriel(),demande.getDemandeur().getIdUtilisateur());
            } else {
                Integer newQuantity = demande.getFourniture().getQuantite() - demande.getQuantite();
                if(newQuantity < 0){
                    throw new RuntimeException("La quantité demandée ("+demande.getQuantite()+") est indisponible");
                } else {
                    fournitureService.updateQuantite(demande.getFourniture().getIdFourniture(),newQuantity);
                }
            }
        }
    }

    @Override
    public void delete(Long id) {
        demandeRepository.deleteById(id);
    }
}
