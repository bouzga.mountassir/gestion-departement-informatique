package com.m2i.service.impl;

import com.m2i.dao.RoleRepository;
import com.m2i.dto.RoleDto;
import com.m2i.enums.RoleName;
import com.m2i.service.RoleService;
import com.m2i.utils.RoleUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public RoleDto findRoleByNom(String nom) {
        return RoleUtils.getInstance().mapEntityToDto(roleRepository.findRoleByNom(RoleName.valueOf(nom)));
    }
}
