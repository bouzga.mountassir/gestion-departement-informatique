package com.m2i.controller.impl;

import com.m2i.controller.DepartementController;
import com.m2i.dto.DepartementDto;
import com.m2i.service.DepartementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartementControllerImpl implements DepartementController {

    private final DepartementService departementService;

    public DepartementControllerImpl(DepartementService departementService) {
        this.departementService = departementService;
    }

    @Override
    public ResponseEntity<DepartementDto> findById(Long id) {
        DepartementDto resultat = departementService.findById(id);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<DepartementDto> save(DepartementDto dto) {
        DepartementDto resultat = departementService.save(dto);
        return ResponseEntity.ok(resultat);
    }
}
