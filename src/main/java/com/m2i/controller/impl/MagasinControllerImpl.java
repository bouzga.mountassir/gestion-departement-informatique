package com.m2i.controller.impl;

import com.m2i.controller.MagasinController;
import com.m2i.dto.MagasinDto;
import com.m2i.service.MagasinService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MagasinControllerImpl implements MagasinController {

    private final MagasinService magasinService;

    public MagasinControllerImpl(MagasinService magasinService) {
        this.magasinService = magasinService;
    }

    @Override
    public ResponseEntity<MagasinDto> findById(Long id) {
        MagasinDto resultat = magasinService.findById(id);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<MagasinDto> save(MagasinDto dto) {
        MagasinDto resultat = magasinService.save(dto);
        return ResponseEntity.ok(resultat);
    }
}
