package com.m2i.controller.impl;

import com.m2i.controller.FournitureController;
import com.m2i.dto.FournitureDto;
import com.m2i.service.FournitureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FournitureControllerImpl implements FournitureController {

    private final FournitureService fournitureService;

    public FournitureControllerImpl(FournitureService fournitureService) {
        this.fournitureService = fournitureService;
    }

    @Override
    public ResponseEntity<FournitureDto> findById(Long id) {
        FournitureDto resultat = fournitureService.findById(id);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<FournitureDto> save(FournitureDto dto) {
        FournitureDto resultat = fournitureService.save(dto);
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<FournitureDto> delete(Long id) {
        FournitureDto dto = fournitureService.findById(id);
        fournitureService.delete(id);
        return ResponseEntity.ok(dto);
    }

    @Override
    public ResponseEntity<List<FournitureDto>> findAll() {
        List<FournitureDto> resultat = fournitureService.findAll();
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<FournitureDto>> findByType(String type) {
        List<FournitureDto> resultat = fournitureService.findByType(type);
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<FournitureDto> update(FournitureDto dto) {
        FournitureDto resultat = fournitureService.update(dto);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }
}
