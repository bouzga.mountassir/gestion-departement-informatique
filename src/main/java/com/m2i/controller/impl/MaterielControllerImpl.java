package com.m2i.controller.impl;

import com.m2i.controller.MaterielController;
import com.m2i.dto.MaterielDto;
import com.m2i.service.MaterielService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MaterielControllerImpl implements MaterielController {

    private final MaterielService materielService;

    public MaterielControllerImpl(MaterielService materielService) {
        this.materielService = materielService;
    }

    @Override
    public ResponseEntity<MaterielDto> findById(Long id) {
        MaterielDto resultat = materielService.findById(id);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<MaterielDto> save(MaterielDto dto) {
        MaterielDto resultat = materielService.save(dto);
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<MaterielDto> delete(Long id) {
        MaterielDto dto = materielService.findById(id);
        materielService.delete(id);
        return ResponseEntity.ok(dto);
    }

    @Override
    public ResponseEntity<MaterielDto> affecterMateriel(Long idMateriel, Long idUtilisateur) {
        MaterielDto resultat = materielService.affecterMateriel(idMateriel,idUtilisateur);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<MaterielDto>> findAll() {
        List<MaterielDto> resultat = materielService.findAll();
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<MaterielDto>> findByMagasinId(Long id) {
        List<MaterielDto> resultat = materielService.findByMagasinId(id);
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<MaterielDto> update(MaterielDto dto) {
        MaterielDto resultat = materielService.update(dto);
        if(resultat == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<MaterielDto>> findMaterielDisponible() {
        List<MaterielDto> resultat = materielService.findMaterielDisponible();
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }

    @Override
    public ResponseEntity<List<MaterielDto>> findByType(String type) {
        List<MaterielDto> resultat = materielService.findByType(type);
        if(resultat == null || resultat.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(resultat);
    }
}
