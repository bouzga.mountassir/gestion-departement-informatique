package com.m2i.controller;

import com.m2i.dto.FournitureDto;
import com.m2i.dto.MaterielDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("Materiel endpoint")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/materiel")
public interface MaterielController {
    /**
     * Chercher un materiel par son ID
     * @param id
     * @return Materiel DTO
     */
    @ApiOperation(value = "Chercher un materiel par son ID")
    @GetMapping(value = "/{id}", produces = "application/json")
    ResponseEntity<MaterielDto> findById(@PathVariable("id") Long id);

    /**
     * Ajouter un materiel
     * @param dto
     * @return ResponseEntity<MaterielDto>
     */
    @ApiOperation(value = "Ajouter un materiel")
    @PostMapping(value = "/add", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<MaterielDto> save(@RequestBody MaterielDto dto);

    /**
     * Supprimer un materiel
     * @param id
     * @return ResponseEntity<MaterielDto>
     */
    @ApiOperation(value = "Supprimer un materiel")
    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<MaterielDto> delete(@PathVariable("id") Long id);

    /**
     * Affecter un materiel
     * @param idMateriel
     * @param idUtilisateur
     * @return ResponseEntity<MaterielDto>
     */
    @ApiOperation(value = "Affecter un materiel")
    @PutMapping(value = "/assign", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<MaterielDto> affecterMateriel(@RequestParam("idMateriel") Long idMateriel,
                                                 @RequestParam("idUtilisateur") Long idUtilisateur);

    /**
     * Récuperer tout le materiel du département
     * @return ResponseEntity<List<MaterielDto>>
     */
    @ApiOperation(value = "Récuperer tout le materiel du département")
    @GetMapping(value = "/all", produces = "application/json")
    ResponseEntity<List<MaterielDto>> findAll();

    /**
     * Récuperer tout le materiel d'un magasin
     * @return ResponseEntity<List<MaterielDto>>
     */
    @ApiOperation(value = "Récuperer tout le materiel d'un magasin")
    @GetMapping(value = "/magasin/{id}", produces = "application/json")
    ResponseEntity<List<MaterielDto>> findByMagasinId(Long id);

    /**
     * Modifier un materiel
     * @return ResponseEntity<MaterielDto>
     */
    @ApiOperation(value = "Modifier un materiel")
    @PutMapping(value = "/update", produces = "application/json")
    ResponseEntity<MaterielDto> update(@RequestBody MaterielDto dto);

    /**
     * Récuperer tout le materiel disponible
     * @return ResponseEntity<List<MaterielDto>>
     */
    @ApiOperation(value = "Récuperer tout le materiel disponible")
    @GetMapping(value = "/disponible", produces = "application/json")
    ResponseEntity<List<MaterielDto>> findMaterielDisponible();

    /**
     * Récuperer un materiel ou plusieurs par type
     * @return ResponseEntity<List<MaterielDto>>
     */
    @ApiOperation(value = "Récuperer un materiel ou plusieurs par type")
    @GetMapping(value = "/type/{type}", produces = "application/json")
    ResponseEntity<List<MaterielDto>> findByType(@PathVariable("type") String type);
}
