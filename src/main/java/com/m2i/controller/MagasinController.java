package com.m2i.controller;

import com.m2i.dto.FournitureDto;
import com.m2i.dto.MagasinDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

@Api("Magasin endpoint")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/magasin")
public interface MagasinController {
    /**
     * Chercher un magasin par son ID
     * @param id
     * @return Magasin DTO
     */
    @ApiOperation(value = "Chercher un magasin par son ID")
    @GetMapping(value = "/{id}", produces = "application/json")
    ResponseEntity<MagasinDto> findById(@PathVariable("id") Long id);

    /**
     * Ajouter un magasin
     * @param dto
     * @return
     */
    @ApiOperation(value = "Ajouter un magasin")
    @PostMapping(value = "/add", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<MagasinDto> save(@RequestBody MagasinDto dto);
}
