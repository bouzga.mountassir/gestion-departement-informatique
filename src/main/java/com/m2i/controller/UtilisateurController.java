package com.m2i.controller;

import com.m2i.dto.UtilisateurDto;
import com.m2i.dto.DepartementDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("Utilisateur endpoint")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/utilisateur")
public interface UtilisateurController {

    /**
     * Chercher un utilisateur par son ID
     * @param id
     * @return Utilisateur DTO
     */
    @ApiOperation(value = "Chercher un utilisateur par son ID")
    @GetMapping(value = "/{id}", produces = "application/json")
    ResponseEntity<UtilisateurDto> findById(@PathVariable("id") Long id);

    /**
     * Chercher la list de tout les utilisateurs
     * @return ResponseEntity<List<UtilisateurDto>>
     */
    @ApiOperation(value = "Chercher la liste de tout les utilisateurs")
    @GetMapping(value = "/all", produces = "application/json")
    ResponseEntity<List<UtilisateurDto>> findAll();

    /**
     * Ajouter un utilisateur
     * @param dto
     * @return UtilisateurDto
     */
    @ApiOperation(value = "Ajouter un utilisateur")
    @PostMapping(value = "/add", produces = "application/json")
    ResponseEntity<Object> save(@RequestBody UtilisateurDto dto);

    /**
     * Modifier un utilisateur
     * @param dto
     * @return UtilisateurDto
     */
    @ApiOperation(value = "Modifier un utilisateur")
    @PutMapping(value = "/update", produces = "application/json")
    ResponseEntity<UtilisateurDto> update(@RequestBody UtilisateurDto dto);

    /**
     * Supprimer un utilisateur
     * @param id
     * @return UtilisateurDto
     */
    @ApiOperation(value = "Supprimer un utilisateur")
    @DeleteMapping(value = "/delete/{id}", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<UtilisateurDto> delete(@PathVariable("id") Long id);

    /**
     * Chercher un utilisateur par son nom
     * @param nom
     * @return ResponseEntity<List<Utilisateur>>
     */
    @ApiOperation(value = "Chercher un utilisateur par son nom")
    @GetMapping(value = "/name/{nom}", produces = "application/json")
    ResponseEntity<List<UtilisateurDto>> findByName(@PathVariable("nom") String nom);

    /**
     * Chercher un utilisateur par email
     * @param email
     * @return ResponseEntity<UtilisateurDto>
     */
    @ApiOperation(value = "Chercher un utilisateur par email")
    @GetMapping(produces = "application/json")
    ResponseEntity<UtilisateurDto> findUtilisateurByEmail(@RequestParam("email") String email);

    /**
     * Changer le chef du departement
     * @param idDepartement
     * @param idChef
     * @return DepartementDto
     */
    @ApiOperation(value = "Changer le chef du departement")
    @PostMapping(value = "/nouveauChef", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<DepartementDto> changerChef(@RequestParam("idDepartement") Long idDepartement,
                                               @RequestParam("idChef") Long idChef);

    /**
     * Changer le chef adjoint
     * @param idDepartement
     * @param idAdjoint
     * @return DepartementDto
     */
    @ApiOperation(value = "Changer le chef du departement")
    @PostMapping(value = "/nouveauAdjoint", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<DepartementDto> changerAdjoint(@RequestParam("idDepartement") Long idDepartement,
                                                  @RequestParam("idAdjoint") Long idAdjoint);

    /**
     * Modifier le mot de passe
     * @param dto
     * @return DepartementDto
     */
    @ApiOperation(value = "Changer le chef du departement")
    @PostMapping(value = "/password", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<UtilisateurDto> updatePassword(@RequestBody UtilisateurDto dto);
}
