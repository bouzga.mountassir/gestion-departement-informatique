package com.m2i.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home Controller pour la redirection automatique vers swagger lors du chergement de projet
 */
@RequestMapping("/")
public interface HomeController {

    /**
     * Redirection vers Swagger UI
     *
     * @return "redirect:/swagger-ui/"
     */
    @GetMapping(value = "${swagger-ui.redirect:}")
    String redirectToSwaggerUi();

}
