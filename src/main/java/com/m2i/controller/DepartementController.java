package com.m2i.controller;

import com.m2i.dto.DepartementDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

@Api("Departement endpoint")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/departement")
public interface DepartementController {

    /**
     * Chercher un departement par son ID
     * @param id
     * @return Departement DTO
     */
    @ApiOperation(value = "Chercher un departement par son ID")
    @GetMapping(value = "/{id}", produces = "application/json")
    ResponseEntity<DepartementDto> findById(@PathVariable("id") Long id);

    /**
     * Ajouter un departement
     * @param dto
     * @return DepartementDto
     */
    @ApiOperation(value = "Ajouter un departement")
    @PostMapping(value = "/add", produces = "application/json")
    //@PostAuthorize("hasAnyAuthority('ADMINISTRATEUR')")
    ResponseEntity<DepartementDto> save(@RequestBody DepartementDto dto);
}
