package com.m2i.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * Configuration des paramètres CORS (Cross-Origin Resource Sharing) pour l'application.
 * La classe WebMvcConfigurer est implémentée pour ajouter des règles de CORS pour toutes les requêtes.
 * La méthode addCorsMappings(CorsRegistry registry) permet de configurer les paramètres CORS.
 * Toutes les requêtes sont autorisées grâce à "/**"  et les méthodes autorisées sont HEAD, GET, PUT, POST, DELETE, PATCH.
 */

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
    }
}
