package com.m2i.enums;

public enum TypeDemande {
    /**
     * les noms de type demande possibles pour un utilisateur sont:
     * FOURNITURE
     * MATERIEL
     */
    FOURNITURE("FOURNITURE"),
    MATERIEL("MATERIEL");

    private final String type;

    TypeDemande(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
