package com.m2i.enums;

/**
 * Grade des enseignants et du technicien
 */
public enum Grade {
    PA("PA"),
    PH("PH"),
    PES("PES"),
    PREMIER("PREMIER"),
    DEUXIEME("DEUXIEME"),
    TROISIEME("TROISIEME");

    private final String grade;

    Grade(final String grade) {
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        return grade;
    }
}
