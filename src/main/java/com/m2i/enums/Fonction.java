package com.m2i.enums;

public enum Fonction {
    /**
     * les noms de fonction possibles pour un utilisateur sont:
     * ENSEIGNANT
     * TECHNICIEN
     */
    ENSEIGNANT("ENSEIGNANT"),
    TECHNICIEN("TECHNICIEN");

    private final String fonction;

    Fonction(final String fonction) {
        this.fonction = fonction;
    }

    public String getGrade() {
        return fonction;
    }

    @Override
    public String toString() {
        return fonction;
    }
}
