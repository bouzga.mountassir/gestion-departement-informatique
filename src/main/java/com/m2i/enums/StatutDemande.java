package com.m2i.enums;

public enum StatutDemande {
    ENCOURS("ENCOURS"),
    APPROUVEE("APPROUVEE"),
    REJETEE("REJETEE"),
    ANNULEE("ANNULEE"),
    ACCEPTEE("ACCEPTEE");

    private final String statut;

    StatutDemande(final String statu) {
        this.statut = statu;
    }

    @Override
    public String toString() {
        return statut;
    }
}
