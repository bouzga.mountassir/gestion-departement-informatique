package com.m2i.enums;


public enum RoleName {
    /**
     * les noms de rôles possibles pour un utilisateur
     */
    ADMINISTRATEUR("ADMINISTRATEUR"),
    UTILISATEUR("UTILISATEUR");

    private final String role;

    RoleName(final String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}
