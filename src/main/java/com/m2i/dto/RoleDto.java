package com.m2i.dto;

import com.m2i.enums.RoleName;
import lombok.*;
/**
 * cette class contient les informations relatives à un rôle d'utilisateur.
 * Elle utilise des annotations de Lombok pour générer automatiquement les accesseurs, les constructeurs...
 * Les champs de cette classe incluent l'identifiant unique du rôle et le nom du rôle (RoleName).
 */


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class RoleDto {
    private Long idRole;
    private RoleName nom;
}
