package com.m2i.dto;
import lombok.*;

import java.time.LocalDate;
/**
 * Cette class contient les informations relatives à un matériel.
 * Elle utilise des annotations de Lombok pour générer automatiquement les accesseurs...
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class MaterielDto {
    private Long idMateriel;
    private Integer numeroInventaire;
    private String type;
    private String dateAcquisition;
    private UtilisateurDto utilisateur;
    private String dateAffectation;
    private MagasinDto magasin;
    private String nomAffectation;
    private String idAffectation;
}
