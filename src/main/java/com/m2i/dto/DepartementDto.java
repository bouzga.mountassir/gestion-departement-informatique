package com.m2i.dto;

import lombok.*;

import java.util.List;
/**
 *Cette class contient les informations relatives à un departement.
 * Elle utilise des annotations de Lombok pour générer automatiquement les accesseurs, les constructeurs...
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class DepartementDto{
    private Long idDepartement;
    private UtilisateurDto chef;
    private UtilisateurDto adjoint;
    private UtilisateurDto technicien;
    private List<UtilisateurDto> enseignants;
    private MagasinDto magasin;
    private String nom;
}
