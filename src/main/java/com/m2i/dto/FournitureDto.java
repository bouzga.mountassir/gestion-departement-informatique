package com.m2i.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
/**
 *Cette class contient les informations relatives à une fourniture.
 * Elle utilise des annotations de Lombok pour générer automatiquement les accesseurs, les constructeurs...
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class FournitureDto {
    private Long idFourniture;
    private String type;
    private Integer quantite;
    @JsonIgnore
    private MagasinDto magasin;
}
