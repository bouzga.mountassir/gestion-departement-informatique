package com.m2i.utils;

import com.m2i.dto.FournitureDto;
import com.m2i.dto.MaterielDto;
import com.m2i.dto.UtilisateurDto;
import com.m2i.entities.Fourniture;
import com.m2i.entities.Materiel;
import com.m2i.entities.Utilisateur;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Singleton en charge des traitements techniques de l'entité et DTO Materiel
 */
public class MaterielUtils {
    private static MaterielUtils instance = null;

    /**
     * Formater des dates pour l'affichage (Front-end)
     */
    private final DateTimeFormatter formattersFront = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    /**
     * Formater des dates pour l'enregistrement dans une base de données.
     */
    private final DateTimeFormatter formattersDB = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * Constructeur privé pour empêcher la création d'instances de la classe en dehors de la classe elle-même
     */
    private MaterielUtils() {
    }

    public static MaterielUtils getInstance() {
        if (instance == null)
            instance = new MaterielUtils();

        return instance;
    }

    /**
     * Mapper (convertit) un objet Materiel en MaterielDto
     * @param entity
     * @return
     */
    public MaterielDto mapEntityToDto(Materiel entity){
        if(entity == null){
            return null;
        }
        UtilisateurDto utilisateur = UtilisateurUtils.getInstance().mapEntityToDto(entity.getUtilisateur());
        String dateAcquisition = entity.getDateAcquisition().format(formattersFront);
        String dateAffectation = entity.getDateAffectation() != null ?
                entity.getDateAffectation().format(formattersFront) : null;
        return MaterielDto.builder()
                .idMateriel(entity.getIdMateriel())
                .numeroInventaire(entity.getNumeroInventaire())
                .type(entity.getType())
                .dateAcquisition(dateAcquisition)
                .dateAffectation(dateAffectation)
                .magasin(MagasinUtils.getInstance().mapEntityToDto(entity.getMagasin()))
                .utilisateur(utilisateur)
                .nomAffectation(utilisateur != null ? utilisateur.getNom()+" "+utilisateur.getPrenom() : null)
                .build();
    }

    /**
     * Mapper (convertit) un objet MaterielDto en Materiel
     * @param dto
     * @return
     */
    public Materiel mapDtoToEntity(MaterielDto dto){
        if(dto == null){
            return null;
        }
        return Materiel.builder()
                .idMateriel(dto.getIdMateriel())
                .numeroInventaire(dto.getNumeroInventaire())
                .type(dto.getType())
                .dateAcquisition(dto.getDateAcquisition() != null ? LocalDateTime.parse(dto.getDateAcquisition(), formattersDB) : null)
                .dateAffectation(dto.getDateAffectation() != null ? LocalDateTime.parse(dto.getDateAffectation(), formattersDB) : null)
                .magasin(MagasinUtils.getInstance().mapDtoToEntity(dto.getMagasin()))
                .utilisateur(UtilisateurUtils.getInstance().mapDtoToEntity(dto.getUtilisateur()))
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets Materiel en une liste d'objets MaterielDto
     * @param entities
     * @return
     */
    public List<MaterielDto> mapEntityToDtoList(List<Materiel> entities){
        if(entities == null){
            return null;
        }
        List<MaterielDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }

    public List<Materiel> mapDtoToEntityList(List<MaterielDto> dtos){
        if(dtos == null){
            return null;
        }
        List<Materiel> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }

    /**
     * Methode qui prend en entrée un objet MaterielDto et un objet Materiel et remplit les champs manquants de l'objet MaterielDto avec les données de l'objet Materiel.
     * @param dto
     * @param entity
     * @return
     */
    public MaterielDto populateDto(MaterielDto dto, Materiel entity){
        if(entity == null){
            return null;
        }

        String dateAcquisition = entity.getDateAcquisition().toString();
        String dateAffectation = entity.getDateAffectation() != null ?
                entity.getDateAffectation().toString() : null;

        if(dto.getIdMateriel() == null){
            dto.setIdMateriel(entity.getIdMateriel());
        }
        if(dto.getType() == null){
            dto.setType(entity.getType());
        }
        if(dto.getDateAcquisition() == null){
            dto.setDateAcquisition(dateAcquisition);
        }
        if(dto.getDateAffectation() == null){
            dto.setDateAffectation(dateAffectation);
        }
        if(dto.getNumeroInventaire() == null){
            dto.setNumeroInventaire(entity.getNumeroInventaire());
        }
        if(dto.getUtilisateur() == null){
            dto.setUtilisateur(UtilisateurUtils.getInstance()
                    .mapEntityToDto(entity.getUtilisateur()));
        }
        if(dto.getMagasin() == null){
            dto.setMagasin(MagasinUtils.getInstance()
                    .mapEntityToDto(entity.getMagasin()));
        }
        return dto;
    }
}
