package com.m2i.utils;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class ApiError {
    /**
     * gérer les erreurs de l'API en stockant des informations telles que:
     * le statut de la réponse
     * le message d'erreur et les détails de l'erreur dans des variables privées pour une utilisation ultérieure
     */

    private HttpStatus status;
    private String message;
    private List<String> errors;

    public ApiError(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }
}
