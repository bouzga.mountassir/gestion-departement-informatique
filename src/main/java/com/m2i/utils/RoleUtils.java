package com.m2i.utils;

import com.m2i.dto.RoleDto;
import com.m2i.entities.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton en charge des traitements techniques de l'entité et DTO Role
 */
public class RoleUtils {
    private static RoleUtils instance = null;
    /**
     * Constructeur privé pour empêcher la création d'instances de la classe en dehors de la classe elle-même
     */
    private RoleUtils() {
    }

    public static RoleUtils getInstance() {
        if (instance == null)
            instance = new RoleUtils();

        return instance;
    }
    /**
     * Mapper (convertit) un objet Role en RoleDto
     * @param entity
     * @return
     */
    public RoleDto mapEntityToDto(Role entity){
        if(entity == null){
            return null;
        }
        return RoleDto.builder()
                .idRole(entity.getIdRole())
                .nom(entity.getNom())
                .build();
    }

    /**
     * Mapper (convertit) un objet RoleDto en Role
     * @param dto
     * @return
     */
    public Role mapDtoToEntity(RoleDto dto){
        if(dto == null){
            return null;
        }
        return Role.builder()
                .idRole(dto.getIdRole())
                .nom(dto.getNom())
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets Role en une liste d'objets RoleDto
     * @param entities
     * @return
     */
    public List<RoleDto> mapEntityToDtoList(List<Role> entities){
        if(entities == null){
            return null;
        }
        List<RoleDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }

    /**
     * Mapper (convertit) une liste d'objets RoleDto en une liste d'objets Role
     * @param dtos
     * @return
     */
    public List<Role> mapDtoToEntityList(List<RoleDto> dtos){
        if(dtos == null){
            return null;
        }
        List<Role> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }
}
