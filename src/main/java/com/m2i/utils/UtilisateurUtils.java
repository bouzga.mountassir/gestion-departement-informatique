package com.m2i.utils;

import com.m2i.dto.UtilisateurDto;
import com.m2i.entities.Utilisateur;
import com.m2i.enums.RoleName;

import java.util.ArrayList;
import java.util.List;

public class UtilisateurUtils {
    private static UtilisateurUtils instance = null;

    /**
     * Constructeur privé pour empêcher la création d'instances de la classe en dehors de la classe elle-même
     */
    private UtilisateurUtils() {
    }

    public static UtilisateurUtils getInstance() {
        if (instance == null)
            instance = new UtilisateurUtils();

        return instance;
    }

    /**
     * Mapper (convertit) un objet Utilisateur en UtilisateurDto
     * @param entity
     * @return
     */
    public UtilisateurDto mapEntityToDto(Utilisateur entity){
        if(entity == null){
            return null;
        }
        return UtilisateurDto.builder()
                .idUtilisateur(entity.getIdUtilisateur())
                .nom(entity.getNom())
                .prenom(entity.getPrenom())
                .cin(entity.getCin())
                .tel(entity.getTel())
                .numBureau(entity.getNumBureau())
                .grade(entity.getGrade())
                .roles(RoleUtils.getInstance().mapEntityToDtoList(entity.getRoles()))
                .email(entity.getEmail())
                .grade(entity.getGrade())
                .fonction(entity.getFonction())
                .isAdmin(entity.getRoles() != null ?
                        entity.getRoles().stream().anyMatch(role -> role.getNom().equals(RoleName.ADMINISTRATEUR)) : false)
                .build();
    }

    /**
     *  Mapper (convertit) un objet UtilisateurDto en Utilisateur
     * @param dto
     * @return
     */
    public Utilisateur mapDtoToEntity(UtilisateurDto dto){
        if(dto == null){
            return null;
        }
        return Utilisateur.builder()
                .idUtilisateur(dto.getIdUtilisateur())
                .nom(dto.getNom())
                .prenom(dto.getPrenom())
                .cin(dto.getCin())
                .tel(dto.getTel())
                .numBureau(dto.getNumBureau())
                .grade(dto.getGrade())
                .roles(RoleUtils.getInstance().mapDtoToEntityList(dto.getRoles()))
                .email(dto.getEmail())
                .password(dto.getPassword())
                .fonction(dto.getFonction())
                .build();
    }

    /**
     * Mapper (convertit) une liste d'objets Utilisateur en une liste d'objets UtilisateurDto
     * @param entities
     * @return
     */
    public List<UtilisateurDto> mapEntityLToDtoList(List<Utilisateur> entities){
        if(entities == null){
            return null;
        }
        List<UtilisateurDto> dtos = new ArrayList<>();
        entities.forEach(element -> dtos.add(mapEntityToDto(element)));
        return dtos;
    }

    /**
     * Mapper (convertit) une liste d'objets UtilisateurDto en une liste d'objets Utilisateur
     * @param dtos
     * @return
     */

    public List<Utilisateur> mapDtoToEntityList(List<UtilisateurDto> dtos){
        if(dtos == null){
            return null;
        }
        List<Utilisateur> entities = new ArrayList<>();
        dtos.forEach(element -> entities.add(mapDtoToEntity(element)));
        return entities;
    }

    /**
     * Methode qui prend en entrée un objet UtilisateurDto et un objet Utilisateur et remplit les champs manquants de l'objet UtilisateurDto avec les données de l'objet Utilisateur
     * @param dto
     * @param entity
     * @return
     */
    public UtilisateurDto populateDto(UtilisateurDto dto,Utilisateur entity){
        if(entity == null){
            return null;
        }
        if(dto.getIdUtilisateur() == null){
            dto.setIdUtilisateur(entity.getIdUtilisateur());
        }
        if(dto.getNom() == null){
            dto.setNom(entity.getNom());
        }
        if(dto.getPrenom() == null){
            dto.setPrenom(entity.getPrenom());
        }
        if(dto.getCin() == null){
            dto.setCin(entity.getCin());
        }
        if(dto.getTel() == null){
            dto.setTel(entity.getTel());
        }
        if(dto.getNumBureau() == null){
            dto.setNumBureau(entity.getNumBureau());
        }
        if(dto.getEmail() == null){
            dto.setEmail(entity.getEmail());
        }
        if(dto.getGrade() == null){
            dto.setGrade(entity.getGrade());
        }
        if(dto.getFonction() == null){
            dto.setFonction(entity.getFonction());
        }
        if(dto.getPassword() == null){
            dto.setPassword(entity.getPassword());
        }
        dto.setRoles(RoleUtils.getInstance().mapEntityToDtoList(entity.getRoles()));
        return dto;
    }
}
