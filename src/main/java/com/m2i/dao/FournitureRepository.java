package com.m2i.dao;

import com.m2i.entities.Fourniture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FournitureRepository extends JpaRepository<Fourniture, Long> {
    /**
     * Chercher une ou plusieurs fourniture par type (complet ou partiel)
     * @param type
     * @return List<Fourniture>
     */
    @Query("select f from Fourniture f where f.type like CONCAT('%', :type, '%')")
    List<Fourniture> findByType(@Param("type") String type);

    /**
     * Supprimer une fourniture
     * @param id
     */
    @Modifying
    @Query("delete from Demande d where d.fourniture.idFourniture = :id ")
    void deleteDemandeByFourniture(@Param("id") Long id);

    @Modifying
    @Query("update Fourniture f set f.quantite = :quantite where f.idFourniture = :id")
    void updateQuantite(@Param("id") Long id, @Param("quantite") Integer quantite);
}
