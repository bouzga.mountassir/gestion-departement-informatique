package com.m2i.dao;

import com.m2i.entities.Materiel;

public interface MaterielCustomRepository {
    /**
     * Affecter un matériel à un utilisateur
     * @param idMateriel
     * @param idUtilisateur
     * @return
     */
    Materiel affecterMateriel(Long idMateriel,Long idUtilisateur);
}
