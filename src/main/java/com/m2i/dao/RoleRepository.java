package com.m2i.dao;

import com.m2i.entities.Role;
import com.m2i.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {
    /**
     * Chercher un role par son nom
     * @param nom
     * @return
     */
    Role findRoleByNom(RoleName nom);

    @Query("SELECT r FROM Role r WHERE r.nom IN (:names)")
    List<Role> findRoleListByNameList(List<RoleName> names);
}
