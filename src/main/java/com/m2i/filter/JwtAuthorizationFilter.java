package com.m2i.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final String AUTHORIZATION = "Authorization";
    private final String BEARER = "Bearer ";
    private final String SECRET = "SECRET";
    private final String REFRESH_TOKEN_RESSOURCE = "/refreshToken";
    private final String ROLE_CLAIM = "roles";
    private final String HEADER_ERROR_MESSAGE = "error-message";

    /**
     * Vérifier si l'utilisateur est autorisé à accéder
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if (request.getServletPath().equals(REFRESH_TOKEN_RESSOURCE)) {
            filterChain.doFilter(request, response);
        } else {
            String authrizationToken = request.getHeader(AUTHORIZATION);
            if (authrizationToken != null && authrizationToken.startsWith(BEARER)) {
                try {
                    String token = authrizationToken.substring(BEARER.length());
                    Algorithm algorithm = Algorithm.HMAC256(SECRET);
                    JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                    DecodedJWT decodedJWT = jwtVerifier.verify(token);
                    String email = decodedJWT.getSubject();
                    List<String> roles = decodedJWT.getClaim(ROLE_CLAIM).asList(String.class);
                    Collection<GrantedAuthority> authorities = new ArrayList<>();
                    roles.forEach(e -> authorities.add(new SimpleGrantedAuthority(e)));
                    UsernamePasswordAuthenticationToken authenticationToken =
                            new UsernamePasswordAuthenticationToken(email, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    filterChain.doFilter(request, response);
                } catch (Exception e) {
                    response.setHeader(HEADER_ERROR_MESSAGE, e.getMessage());
                    response.sendError(HttpServletResponse.SC_FORBIDDEN);
                }

            } else {
                filterChain.doFilter(request, response);
            }
        }
    }
}
